import machine
import utime

led_onboard = machine.Pin(25, machine.Pin.OUT)
pause_seconds = 5

while True:
    led_onboard.value(1)
    utime.sleep(pause_seconds)
    led_onboard.value(0)
    utime.sleep(pause_seconds)
